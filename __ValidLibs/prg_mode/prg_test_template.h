/*----------------------------------------------------------------------------
*         ATMEL Microcontroller Software Support  -  ROUSSET  -
*----------------------------------------------------------------------------
* The software is delivered "AS IS" without warranty or condition of any
* kind, either express, implied or statutory. This includes without
* limitation any warranty or condition with respect to merchantability or
* fitness for any particular purpose, or against the infringements of
* intellectual property rights of others.
*----------------------------------------------------------------------------
* File Name           : 
* Object              : 
*
* Creation            : 
*----------------------------------------------------------------------------
*/
#ifndef _PRG_TEST_TEMPLATE_H
#define _PRG_TEST_TEMPLATE_H





// -- Misc definitions --------------------------------------------------------
// ----------------------------------------------------------------------------


// -- Prototypes --------------------------------------------------------------
//uint32_t prg_dummy_test();
// ----------------------------------------------------------------------------


#endif //_PRG_TEST_TEMPLATE_H

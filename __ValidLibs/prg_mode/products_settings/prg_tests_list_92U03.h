/*----------------------------------------------------------------------------
*         ATMEL Microcontroller Software Support  -  ROUSSET  -
*----------------------------------------------------------------------------
* The software is delivered "AS IS" without warranty or condition of any
* kind, either express, implied or statutory. This includes without
* limitation any warranty or condition with respect to merchantability or
* fitness for any particular purpose, or against the infringements of
* intellectual property rights of others.
*----------------------------------------------------------------------------
* File Name           : prg_tests_list.h
* Object              : 
*
* Creation            : ERr / Jan 2015
*----------------------------------------------------------------------------
*/
/*---------------------------------------------------------------------------
*----------------------------------------------------------------------------
* Product compliance (tested):

*----------------------------------------------------------------------------
*/
#ifndef _PRG_TESTS_LIST_92U03_H
#define _PRG_TESTS_LIST_92U03_H


// -- Test identifiers --------------------------------------------------------
#define PRG_ID00_TEST   prg_reserved_test
#define PRG_ID01_TEST   prg_low_power_idd_test
#define PRG_ID02_TEST   prg_dynamic_idd_test
#define PRG_ID03_TEST   prg_rc_measure_test
#define PRG_ID04_TEST   prg_DAC12_test
#define PRG_ID05_TEST   prg_AFE_test
//#define PRG_ID06_TEST   prg_user06_test
//...
//#define PRG_ID30_TEST   prg_user30_test
#define PRG_ID31_TEST   prg_non_reg_test
// ----------------------------------------------------------------------------

#endif //_PRG_TESTS_LIST_92U03_H
/*! \file *********************************************************************
For ASF libs compliance
*******************************************************************************/
#ifndef _COMPILER_H
#define _COMPILER_H

//#include "parts.h"
#include "project.h"
#include "device.h"

//#ifdef __ICCARM__
//#endif


#undef __cplusplus


#ifndef __cplusplus
#if !defined(__bool_true_false_are_defined)
typedef unsigned char           bool; //!< Boolean.
#define false     0
#define true      1
#endif
#endif


typedef int8_t                  S8 ;  //!< 8-bit signed integer.
typedef uint8_t                 U8 ;  //!< 8-bit unsigned integer.
typedef int16_t                 S16;  //!< 16-bit signed integer.
typedef uint16_t                U16;  //!< 16-bit unsigned integer.
typedef uint16_t                le16_t;
typedef uint16_t                be16_t;
typedef int32_t                 S32;  //!< 32-bit signed integer.
typedef uint32_t                U32;  //!< 32-bit unsigned integer.
typedef uint32_t                le32_t;
typedef uint32_t                be32_t;
typedef signed long long int    S64;  //!< 64-bit signed integer.
typedef unsigned long long int  U64;  //!< 64-bit unsigned integer.
typedef float                   F32;  //!< 32-bit floating-point number.
typedef double                  F64;  //!< 64-bit floating-point number.
typedef uint32_t                iram_size_t;


// Soft pack compatibility
#ifdef _SOFTPACK_INCLUDED_ // point to softpack header
  #include "sp_compiler.h"
#endif //_SOFTPACK_INCLUDED_


#endif  // _COMPILER_H
